var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var adminsService = require('../services/adminsAPI');

router.post('/create', function(req, res){
	adminsService.create(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.admin);
	})
});

router.post('/find', function(req, res) {

	adminsService.find(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.admin);
	})

});

/*router.post('/update', function(req, res) {

	adminsService.update(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.admin);
	})

});*/

router.post('/update/settings', function(req, res) {

	adminsService.updateSettings(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.admin);
	})

});

module.exports = router;
