var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var http = require('http');
var streamsService = require('../services/streamsAPI');
var requestclient = require('request');

var parseBody = function(body) {
	var responseArray = [];
		if(typeof body == 'string'){
			var body = JSON.parse(body);
			for( var key in body) {
				var obj = body[key]
				for(iKey in obj) {
					obj[iKey].quality = key.slice(key.lastIndexOf('/') +1, key.length)
					responseArray.push(obj[iKey])
				}
			}
			return responseArray;
		}else{
			return responseArray;
		}
}

router.post('/check', function(req, res){
	var notautentication = (req.body.user == undefined || req.body.user == null) && (req.body.pass == undefined || req.body.pass == null);
	if(notautentication){
		var url = req.body.url;
		requestclient(
			{url: url, 
			qs:{aspects: 'live-status-local', dateformat:'plain',format:'application/json'},
			timeout: 5000
			}, function (error, response, body) {
			if(error) {
				return res.send(500);
			} else if( body.indexOf("<html>") != -1){
				res.send(401);
			} else {
				res.send(parseBody(body));
			}
	   		
		});

	}else{
		var username = req.body.user,
	    password = req.body.pass,
	    url = req.body.url;
		requestclient(
			{url: url, 
			auth : {user: req.body.user, pass: req.body.pass, sendImmediately: false}, 
			qs:{aspects: 'live-status-local', dateformat:'plain',format:'application/json'},
			timeout: 5000
			}, function (error, response, body) {
			if(error) {
				return res.send(500);
			} else if( body.indexOf("<html>") != -1){
				res.send(401);
			} else {
				res.send(parseBody(body));
			}
	   		
		});
	}

})

module.exports = router;
