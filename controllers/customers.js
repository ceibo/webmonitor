var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var customersService = require('../services/customersAPI');

router.post('/create', function(req, res){
	customersService.create(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.customer);
	})
});

router.post('/update', function(req, res){
	customersService.update(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.customer);
	})
});

router.post('/delete', function(req, res){
	customersService.delete(req.body, function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response);
	})
});

router.get('/retrieveAll', function(req, res) {

	streamsService.retrieveAll(function(response){
		if(response.err) {
			res.send(500);
		}
		res.send(response.items);
	})

});

module.exports = router;