var mongoose = require('mongoose');
var Schema = require('mongoose').Schema;

var streamSchema = new Schema({
	stream: String,
	IP: String,
	port: String,
	BCCVersion: String,
	name: String,
	playerUrl: String
})

module.exports = mongoose.model('Stream', streamSchema);