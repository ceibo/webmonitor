var mongoose = require('mongoose');
var Schema = require('mongoose').Schema;

var adminSchema = new Schema({
	name : String,
	customers : [{type: Schema.Types.ObjectId, ref: 'Customer'}],
	settings: { type: Object, default: { 
		loop: 5000
	}}
});

module.exports = mongoose.model('Admin', adminSchema);