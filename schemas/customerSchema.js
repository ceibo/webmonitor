var mongoose = require('mongoose');
var Schema = require('mongoose').Schema;

var customerSchema = new Schema({
	createdBy: {type: Schema.Types.ObjectId, ref: 'Admin' },
	name : String,
	streams : []
});

module.exports = mongoose.model('Customer', customerSchema);