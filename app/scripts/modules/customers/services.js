angular
  .module('customersModule')

  .service('customersService', ['$http','apiBaseUrl', function ($http, apiBaseUrl) {
  	
  	var customersBaseUrl = apiBaseUrl + '/customers';

  	this.create = function(customer) {
  		return $http.post(customersBaseUrl + '/create', customer)
  	};

    this.update = function(customer) {
      return $http.post(customersBaseUrl + '/update', customer)
    };

    this.delete = function(customer) {
      return $http.post(customersBaseUrl + '/delete', customer)
    };
  	
  }])

  .service('streamsService', ['$http','apiBaseUrl', function ($http, apiBaseUrl) {
  	
    var streamsBaseUrl = apiBaseUrl + '/streams';

  	this.checkStatus = function(stream) {
      var options =  { 
        fullurl: 'http://' + stream.IP + ':' + stream.port + '/hugin/' + stream.BCCVersion + '/local/' + stream.streamUrl + '*?aspects=live-status-local&dateformat=plain&format=application/json',
        url: 'http://' + stream.IP + ':' + stream.port + '/hugin/' + stream.BCCVersion + '/local/'+ stream.streamUrl + '*',
        port: stream.port,
        user: stream.user,
        pass: stream.pass
      };
      
  		return $http.post(streamsBaseUrl + '/check', options);
  	}

  }])