angular.module('dashBoardModule')
	.controller('dashBoardCtrl', [
		'$scope',
		'adminService',
		'customersService',
		'streamsService',
		'messageBoxService',
		'$modal',
		'$timeout',
 function ($scope, adminService, customersService, streamsService, messageBoxService, $modal, $timeout) {
 		//Init of filter object

 		$scope.filter =  {
 			name : null 
 		};

 		function init() {
 			$scope.loading = true;
			$scope.timeOptions = {
				loop : parseMillisecondsToString($scope.admin.settings.loop)
			};

			//Loop for check streams
			intervalFunction()
 		}

 		//get if the stream has error in some level
 		var checkIfStreamHasError = function(streamsStatus) {
 			for (var i = 0; i < streamsStatus.length; i++) {
 				if(streamsStatus[i].status == 'error') {
 					return  true;
 				} 
 			};
 			return false;

 		}

 		//Function to check the channels status
 		var checkStreamStatus = function(stream, customer) {
 			streamsService.checkStatus(stream)
 				.success(function(response){
 					if(response.length) {
	 					stream.statusObject = response;
	 					customer.hasError = checkIfStreamHasError(response);

	 					customer.hasConnectionError = false;
	 					stream.hasConnectionError = false;

	 					customer.hasAuthorizationError = false;
	 					stream.hasAuthorizationError = false;	

 					} else {

	 					stream.hasConnectionError = true;
 						customer.hasConnectionError = true;
 					}

 					$scope.loading = false;
 				})
 				.error(function(response){
 					if(response == 'Unauthorized') {
 						customer.hasAuthorizationError = true;
 						stream.hasAuthorizationError = true;		

 					} else {
	 					customer.hasConnectionError = true
	 					stream.hasConnectionError = true;
 					}
 					
 					$scope.loading = false;
 				})
 		};

 		/*Modals */

 		var addCustomerModal = $modal(
 			{
 				scope: $scope, template: 'views/customer-modal.html' , show: false
 			});

 		/**Modals Actions **/

 		$scope.openAddCustomerModal = function(customer) {
 			addCustomerModal.$scope.customer = {
				createdBy : $scope.admin._id,
				streams: []
 			}
 			addCustomerModal.$scope.updateMode = false;
 			addCustomerModal.show();
 		}

 		
 		$scope.openCustomerModal = function(customer) {
 			addCustomerModal.$scope.customer= {};
 			angular.copy(customer, addCustomerModal.$scope.customer)
 			addCustomerModal.$scope.updateMode = true;
 			addCustomerModal.show();
 		}

		/***Actions ***/
 		
		var intervalFunction = function(){
		    $timeout(function() {
		      
		    	if($scope.admin.customers) {
		    		angular.forEach($scope.admin.customers, function(customer){
		    			angular.forEach(customer.streams, function(stream){
		    				checkStreamStatus(stream, customer);
		    			})
		    		})
		    	}

		      intervalFunction();
		    },  $scope.admin.settings.loop)
		  };



		$scope.findAdmin = function(admin) {
			adminService.findAdmin(admin)
				.success(function(response){
					if(!response) {
						$scope.showAlert = true
					} else {
						$scope.admin = response;
					}

					init();
				})
		};

		$scope.createAdmin = function(admin) {
			adminService.createAdmin(admin)
				.success(function(response){
					if(response != 'exists') {
						$scope.admin = response;
						init();
					} else {
						$scope.userExists = true;
					}
				})
		};


		$scope.saveCustomer = function(customer) {
			customersService.create(customer)
				.success(function(response){
					messageBoxService.showSuccess('The customer has been saved successfully')
					$scope.admin.customers.push(response);
				})

				.error(function(){
					messageBoxService.showError('An error has occured');
				})
		};

		$scope.updateCustomer = function(customer) {
			customersService.update(customer)
				.success(function(response){
					for (var i = 0; i < $scope.admin.customers.length; i++) {
						if($scope.admin.customers[i]._id == customer._id) {
							return $scope.admin.customers[i] = customer
						}
					};
					messageBoxService.showSuccess('The changes in the customer has been saved successfully')
				})

				.error(function(){
					messageBoxService.showError('An error has occured');
				})
		};


		$scope.addStream = function(newStream) {
			$scope.customer.streams.push(newStream);
		}

		$scope.customersFilterFn = function(customer) {
		    if($scope.filter.name && customer.name && (customer.name.toLowerCase().indexOf($scope.filter.name.toLowerCase()) == -1) ) {
		        return false;
		    };

		    return true;
		};

		$scope.deleteElement = function(list, obj){
			var index = list.indexOf(obj);
			list.splice(index,1)
		}

		$scope.deleteCustomer = function (customer){
			customersService.delete(customer)
				.success(function(response){
					var customerIndex = $scope.admin.customers.indexOf(customer);
					$scope.admin.customers.splice(customerIndex, 1);
					messageBoxService.showSuccess('The customer has been removed successfully');
				})

				.error(function(){
					messageBoxService.showError('An error has occured');
				})
		}

		$scope.checkStreamBeforeAdd = function(stream) {
			$scope.checkingStreamStatus = true;
			streamsService.checkStatus(stream)
				.success(function(response){
					if(response.length) {
 					$scope.validStream = true
 					$scope.invalidStream = false;
					} else {
						$scope.validStream = false;	
						$scope.invalidStream = true;
					}
				$scope.checkingStreamStatus = false;
				})
				.error(function(response){
					$scope.validStream = false;	
					$scope.invalidStream = true;	
					$scope.checkingStreamStatus = false;
				})
		}

		//Configuration Aside
		var conversionUnits = {
			oneHour : 3600000,
			oneMinute : 60000,
			oneSecond : 1000
		}

		$scope.newSettings = {
			loop: null
		}

		var parseStringToMilliseconds = function(string) {
			valuesArray = string.split(':')
			return milliseconds = parseInt(valuesArray[0]) * conversionUnits.oneHour + parseInt(valuesArray[1]) * conversionUnits.oneMinute + parseInt(valuesArray[2]) * conversionUnits.oneSecond

		}

		var parseMillisecondsToString = function(milliseconds) {
			var hour, minutes, seconds;
			hours = Math.floor(milliseconds / conversionUnits.oneHour);
			milliseconds -= hours * conversionUnits.oneHour; 
			minutes = Math.floor(milliseconds / conversionUnits.oneMinute); 
			milliseconds -= minutes * conversionUnits.oneMinute; 
			seconds = Math.floor(milliseconds / conversionUnits.oneSecond); 
			return "" + ( (hours < 10) ? "0"+ hours : hours)  + ":" + ( (minutes < 10) ?  "0" + minutes : minutes)+ ":" + ( (seconds < 10) ? "0" + seconds : seconds)
		}

		$scope.$watch('timeOptions.loop', function(value) {
			if(value) {
				$scope.newSettings.loop = parseStringToMilliseconds(value);
			}
		})
		
		$scope.saveSettings = function(admin, settings) {
			adminService.updateSettings(admin, settings)
				.success(function(admin){
					$scope.admin.settings = admin.settings;
					
					//This must be refactored, this was a solution ad hoc.
					//The problem has some considerations, I might find a better solutions, is not so trivial like is seems.
					intervalFunction = null;
					var intervalFunction = function(){
					    $timeout(function() {
					      
					    	if($scope.admin.customers) {
					    		angular.forEach($scope.admin.customers, function(customer){
					    			angular.forEach(customer.streams, function(stream){
					    				checkStreamStatus(stream, customer);
					    			})
					    		})
					    	}

					      intervalFunction();
					    },  $scope.admin.settings.loop)
					  };

					messageBoxService.showSuccess('The new settings has been saved successfully');
				})

				.error(function(){
					messageBoxService.showError('An error has occured');
				})
		}

		/***Patterns **/
		$scope.timePattern = /^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/;
		$scope.urlPattern = /^([\s\S]+\/)+$/
	}])