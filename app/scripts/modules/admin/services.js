 angular
  .module('adminModule')
  .service('adminService',['$http', 'apiBaseUrl', function ($http, apiBaseUrl) {
  	
  	adminBaseUrl = apiBaseUrl + '/admin';

  	this.createAdmin = function(admin) {
  		return $http.post(adminBaseUrl + '/create', admin)
  	};
  	
  	this.findAdmin = function(admin) {
  		return $http.post(adminBaseUrl + '/find', admin)
  	};

    this.updateSettings = function(admin, settings) {
      var request = {
        admin: admin,
        settings: settings
      };
      
      return $http.post(adminBaseUrl + '/update/settings', request)
    }

  	
  }]) 