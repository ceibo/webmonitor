'use strict';

angular
  .module('webMonitorApp', [
    'ui.router',
    'ceibo.ui',
    'mgcrea.ngStrap', 
    'ngAnimate',
    'dashBoardModule',
    'adminModule',
    'customersModule'])
  
  .constant('apiBaseUrl', 'http://' + window.location.host)

  .config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      
      $urlRouterProvider.otherwise('/');
    
      $stateProvider
        .state('control-panel', {
          url: '/',
          templateUrl: 'views/control-panel.html',
          controller: 'dashBoardCtrl'
        })
  }])