var mongoose = require('mongoose');
var Customer = require('../schemas/customerSchema');
var Admin = require('../schemas/adminSchema');

//PUBLIC

var customersAPI =  {
	create : function(customer, callback) {
		newCustomer = new Customer(customer);
		newCustomer.save(function(err, customer) {
		  if (err) return callback({err : err});
		  Admin
		  	.findOne({_id : customer.createdBy}, function(error, admin){
		  		if (err) return callback({err : err});
		  		admin.customers.push(customer._id);
		  		admin.save(function(err, admin){
		  			if (err) return callback({err : err});
		  			callback({customer: customer})	
		  		});
		  		
		  	})
		});
	},

	delete : function(customer, callback) {
		Customer
			.findById(customer._id)
			.exec(function(err, customerToRemove){
				customerToRemove.remove(function(err, removedCustomer){
					if( err) return  callback(err);
					Admin.findById(customer.createdBy, function(err, refAdmin){
						var customerIndex = refAdmin.customers.indexOf(customer._id);
						refAdmin.customers.splice(customerIndex,1);
						refAdmin.save();
						return callback(200)
					})
				})
			})
	},

	update : function(customer, callback) {
		Customer.findByIdAndUpdate(customer._id, { $set: { name: customer.name, streams : customer.streams }}, function (err, updatedCustomer) {
			  if (err) return callback({err: err});
			  callback({customer: updatedCustomer});
		});
	},
}



module.exports= customersAPI;