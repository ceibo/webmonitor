var mongoose = require('mongoose');
var configuration = {
	URI: process.env.MONGOLAB_URI || 'mongodb://localhost',
	dbName: '/monitor-db'
}

var db = {
 	connect : function() {
 		mongoose.connect(configuration.URI + configuration.dbName);
 	},

 	disconnect : function() {
 		mongoose.disconnect();
 		console.log('disconnected');
 	}
}


//Configure and expose the database

module.exports = db;