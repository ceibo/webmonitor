var mongoose = require('mongoose');
var Item = require('../schemas/streamSchema');

//PRIVATE
var updateItemsListIterator = function(item) {
	Item.findById(item._id, function (err, foundItem) {
		  if (err) return (err);
		  if(item.quantity != foundItem.quantity) {
		  	 foundItem.quantity = item.quantity;
		  	 foundItem.save();
		  }
		});
};

//PUBLIC

var itemsAPI =  {
	createItem : function(item, callback) {
		newItem = new Item(item);
		newItem.save(function(err, item) {
		  if (err) return callback({err : err});
		  callback({item: item})
		});
	},

	findItems: function(query, callback) {
		Item.find(query, function(err, items){
			if (err) return callback({err : err});
			callback({items: items})
		})
	},

	retrieveAll : function(callback) {
		Item.find({}, function(err, items){
			if (err) return callback({err : err});
			callback({items: items})
		})
	},

	updateItemsList : function(availableItems, callback) {
		for (var i = 0; i < availableItems.length; i++) {
			updateItemsListIterator(availableItems[i]);
		};

		callback(200);
	}
}



module.exports= itemsAPI;



	