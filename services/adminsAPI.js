var mongoose = require('mongoose');
var Admin = require('../schemas/adminSchema');

var adminAPI =  {
	create : function(admin, callback) {
		Admin
			.findOne({name : admin.name})
			.exec(function(err, foundAdmin){
				if(!foundAdmin) {
					newAdmin = new Admin(admin);
					newAdmin.save(function(err, admin) {
					  if (err) return callback({err : err});
					  callback({admin: admin})
					});
				} else {
					callback({admin : 'exists'})
				}
			})

	},

	find: function(query, callback) {
		Admin
			.findOne(query)
			.populate('customers')
			.exec(function(err, admin){
				if (err) return callback({err : err});
				callback({admin: admin})
			})
	},

	updateSettings : function(request, callback) {
		Admin.findByIdAndUpdate(request.admin._id, { $set : { settings: request.settings } }, function(err, admin){
			if (err) return callback({err : err});
			callback({admin: admin})
		})

	},

}

module.exports= adminAPI;